#include "bench-common.hpp"

int main() {
  print_benchmark();
  extern_print_device_info();

  std::vector<double> stats_gmem_fp32 = run_gmem_fp32_v1();
  print_stats("GMFP32", stats_gmem_fp32, 1);
  std::vector<double> stats_gmem_fp64 = run_gmem_fp64_v1();
  print_stats("GMFP64", stats_gmem_fp64, 1);

  std::vector<double> stats_l2_fp32;
  std::vector<double> stats_l2_fp64;
  std::vector<double> stats_l1_fp32;
  std::vector<double> stats_l1_fp64;
#ifndef BUILD_OPENCL
#ifdef BUILD_CUDA
  stats_l2_fp32 = run_l2_fp32_v1();
  print_stats("L2FP32", stats_l2_fp32, 1);
  stats_l2_fp64 = run_l2_fp64_v1();
  print_stats("L2FP64", stats_l2_fp64, 1);
#endif
  stats_l1_fp32 = run_l1_fp32_v1();
  print_stats("L1FP32", stats_l1_fp32, 1);
  stats_l1_fp64 = run_l1_fp64_v1();
  print_stats("L1FP64", stats_l1_fp64, 1);
#endif

  std::vector<double> stats_fp16;
  try {
    stats_fp16 = run_fp16_2_v1();
    print_stats("FP16", stats_fp16, 0);
  } catch (const std::exception &e) {
    std::cout << "FP16 not supported" << std::endl;
  }
  std::vector<double> stats_fp32 = run_fp32_v1();
  print_stats("FP32", stats_fp32, 0);
  std::vector<double> stats_fp64 = run_fp64_v1();
  print_stats("FP64", stats_fp64, 0);
}
