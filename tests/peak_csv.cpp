#include "bench-common.hpp"

int main(int argc, char *argv[]) {
  print_benchmark();
  extern_print_device_info();

  std::vector<std::string> units = {"GFLOP/s", "GB/s"};

  std::vector<std::string> titles = {"GMV1FP32", "GMV1FP64","GMV2FP32", "GMV2FP64", "L2FP32",
                                     "L2FP64", "L1FP32", "L1FP64",
                                     "FP16",   "FP32",   "FP64"};
  std::vector<int> selectors = {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0};
  std::vector<std::vector<double>> stats;

  std::vector<double> stats_gmem_fp32_v1 = run_gmem_fp32_v1();
  print_stats("GMV1FP32", stats_gmem_fp32_v1, 1);
  std::vector<double> stats_gmem_fp64_v1 = run_gmem_fp64_v1();
  print_stats("GMV1FP64", stats_gmem_fp64_v1, 1);

  std::vector<double> stats_gmem_fp32_v2 = run_gmem_fp32_v2();
  print_stats("GMV2FP32", stats_gmem_fp32_v2, 1);
  std::vector<double> stats_gmem_fp64_v2 = run_gmem_fp64_v2();
  print_stats("GMV2FP64", stats_gmem_fp64_v2, 1);

  std::vector<double> stats_l2_fp32(3, 0.0);
  std::vector<double> stats_l2_fp64(3, 0.0);
  std::vector<double> stats_l1_fp32(3, 0.0);
  std::vector<double> stats_l1_fp64(3, 0.0);
#ifndef BUILD_OPENCL
#ifdef BUILD_CUDA
  stats_l2_fp32 = run_l2_fp32_v1();
  print_stats("L2FP32", stats_l2_fp32, 1);
  stats_l2_fp64 = run_l2_fp64_v1();
  print_stats("L2FP64", stats_l2_fp64, 1);
#endif
  stats_l1_fp32 = run_l1_fp32_v1();
  print_stats("L1FP32", stats_l1_fp32, 1);
  stats_l1_fp64 = run_l1_fp64_v1();
  print_stats("L1FP64", stats_l1_fp64, 1);
#endif

  std::vector<double> stats_fp16(3, 0.0);
  bool fail_fp16 = false;
  try {
    stats_fp16 = run_fp16_2_v1();
    print_stats("FP16", stats_fp16, 0);
  } catch (const std::exception &e) {
    std::cout << "FP16 not supported" << std::endl;
    fail_fp16 = true;
  }
  std::vector<double> stats_fp32 = run_fp32_v1();
  print_stats("FP32", stats_fp32, 0);
  std::vector<double> stats_fp64 = run_fp64_v1();
  print_stats("FP64", stats_fp64, 0);

  stats.push_back(stats_gmem_fp32_v1);
  stats.push_back(stats_gmem_fp64_v1);
  stats.push_back(stats_gmem_fp32_v2);
  stats.push_back(stats_gmem_fp64_v2);
  stats.push_back(stats_l2_fp32);
  stats.push_back(stats_l2_fp64);
  stats.push_back(stats_l1_fp32);
  stats.push_back(stats_l1_fp64);
  stats.push_back(stats_fp16);
  stats.push_back(stats_fp32);
  stats.push_back(stats_fp64);

  std::string file_path;
  if (argc != 2) {
    std::cout << std::endl;
    std::cout
        << ">>> User did not set filepath, saving results in current folder"
        << std::endl;
    file_path = "./";
  } else {
    file_path = argv[1];
  }

  std::ofstream output;
  std::string file_extension;
#ifdef BUILD_CUDA
  file_extension = "_cuda_peak.csv";
#elif BUILD_OPENCL
  file_extension = "_opencl_peak.csv";
#else
  file_extension = "_hip_peak.csv";
#endif
  output.open(file_path + "/" + extern_get_device_name() + file_extension);
  output << std::fixed << std::setprecision(2);
  for (int i = 0; i < titles.size(); i++) {
    output << titles[i] << "," << stats[i][2] << "," << units[selectors[i]]<< "\n";
  }
  output.close();
}
