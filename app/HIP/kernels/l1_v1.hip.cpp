#include "../util.hip.hpp"

#define ARRAY_SIZE 64 * 1024
#define THREADS_NUM 1024
#define BLOCK_SIZE 32

template <typename T>
__global__ void kernel_l1_v1(uint32_t *clk, T *dsink, uint32_t *posArray) {
  uint32_t tid = threadIdx.x;
  T sink = 0;

  for (uint32_t i = tid; i < ARRAY_SIZE; i += THREADS_NUM) {
    T *ptr = reinterpret_cast<T *>(posArray) + i;
    T data;
    data = *(ptr);
    sink += data;
  }

  __syncthreads();

  uint32_t start = clock();

  for (uint32_t i = 0; i < ARRAY_SIZE; i += THREADS_NUM) {
    T *ptr = reinterpret_cast<T *>(posArray) + i;
    for (uint32_t j = 0; j < THREADS_NUM; j += BLOCK_SIZE) {
      uint32_t offset = (tid + j) % THREADS_NUM;
      T data;
      data = *(ptr + offset);
      sink += data;
    }
  }

  __syncthreads();

  uint32_t stop = 0;
  stop = clock();

  clk[tid] = stop - start;
  dsink[tid] = sink;
}

template <typename T> std::vector<double> run_l1_v1() {
  std::vector<int> dim = get_launch_kernel_dimensions();
  int nr_warm_up_runs = get_env_var("NR_WARM_UP_RUNS", 2);
  int nr_iterations = get_env_var("NR_ITERATIONS", 5);

  int cu_freq = get_cu_freq();
  int size = 1 * dim[1];

#ifdef DEBUG
  print_device_info();
  std::cout << "Dimensions: <<" << dim[0] << ", " << dim[1] << ">>"
            << std::endl;
  std::cout << "NR_WARM_UP_RUNS: " << nr_warm_up_runs << std::endl;
  std::cout << "NR_ITERATIONS: " << nr_iterations << std::endl;
#endif

  T *d_sink;
  uint32_t *d_posArray;
  uint32_t *clk, *d_clk;

  clk = (uint32_t *)malloc(THREADS_NUM * sizeof(uint32_t));

  hipEvent_t start, stop;
  hipCheck(hipEventCreate(&start));
  hipCheck(hipEventCreate(&stop));

  hipCheck(hipMalloc(&d_sink, ARRAY_SIZE * sizeof(T)));
  hipCheck(hipMalloc(&d_posArray, ARRAY_SIZE * sizeof(uint32_t)));
  hipCheck(hipMalloc(&d_clk, THREADS_NUM * sizeof(uint32_t)));

  std::vector<float> clks;

  for (int i = 0; i < nr_iterations + nr_warm_up_runs; i++) {
    hipCheck(hipEventRecord(start));

    hipLaunchKernelGGL(kernel_l1_v1, 1, dim[1], 0, 0, d_clk, d_sink,
                       d_posArray);

    hipCheck(hipEventRecord(stop));

    hipCheck(hipEventSynchronize(stop));

    float milliseconds = 0;
    hipCheck(hipEventElapsedTime(&milliseconds, start, stop));

    hipMemcpy(clk, d_clk, THREADS_NUM * sizeof(uint32_t),
              hipMemcpyDeviceToHost);

    clks.push_back(clk[100]);
  }

  hipCheck(hipFree(d_sink));
  hipCheck(hipFree(d_posArray));
  hipCheck(hipFree(d_clk));

  float avg_ex_time =
      (float)std::accumulate(clks.begin() + nr_warm_up_runs, clks.end(), 0.0) /
      (clks.size() - nr_warm_up_runs) / (cu_freq * 1e3);

  double byte = dim[0] * THREADS_NUM * sizeof(T) *
                ((ARRAY_SIZE / THREADS_NUM) * (THREADS_NUM / BLOCK_SIZE)) *
                1e-9;
  double gbytes = byte / avg_ex_time;

  return {(double)avg_ex_time, byte * 1e-9, gbytes};
}

std::vector<double> run_l1_fp32_v1() { return run_l1_v1<float>(); }

std::vector<double> run_l1_fp64_v1() { return run_l1_v1<double>(); }
