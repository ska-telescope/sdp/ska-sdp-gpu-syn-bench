#include "../util.hip.hpp"

template <typename T> __global__ void kernel_gmem_v2(T *a, T *c) {
  int id = (blockIdx.x * blockDim.x) + threadIdx.x;
  c[id] = a[id];
}

template <typename T> std::vector<double> run_gmem_v2() {
  std::vector<int> dim = get_launch_kernel_dimensions();
  int nr_warm_up_runs = get_env_var("NR_WARM_UP_RUNS", 2);
  int nr_iterations = get_env_var("NR_ITERATIONS", 5);

  int maxItems = get_gmem_size() / 3;
  float numItems = (float)roundToPowOf2(maxItems) / sizeof(T);
  dim[0] = numItems / dim[1];
  int size = dim[0] * dim[1];

#ifdef DEBUG
  print_device_info();
  std::cout << "Dimensions: <<" << dim[0] << ", " << dim[1] << ">>"
            << std::endl;
  std::cout << "NR_WARM_UP_RUNS: " << nr_warm_up_runs << std::endl;
  std::cout << "NR_ITERATIONS: " << nr_iterations << std::endl;
#endif

  T *d_ptr1, *d_ptr2;

  hipEvent_t start, stop;
  hipCheck(hipEventCreate(&start));
  hipCheck(hipEventCreate(&stop));

  hipCheck(hipMalloc(&d_ptr1, size * sizeof(T)));
  hipCheck(hipMalloc(&d_ptr2, size * sizeof(T)));

  std::vector<float> ex_time;

  for (int i = 0; i < nr_iterations + nr_warm_up_runs; i++) {
    hipCheck(hipEventRecord(start));

    kernel_gmem_v2<<<dim[0], dim[1]>>>(d_ptr1, d_ptr2);

    hipCheck(hipEventRecord(stop));

    hipCheck(hipEventSynchronize(stop));

    float milliseconds = 0;
    hipCheck(hipEventElapsedTime(&milliseconds, start, stop));

    ex_time.push_back(milliseconds);
  }

  hipCheck(hipFree(d_ptr1));
  hipCheck(hipFree(d_ptr2));

  float avg_ex_time =
      std::accumulate(ex_time.begin() + nr_warm_up_runs, ex_time.end(), 0.0) /
      (ex_time.size() - nr_warm_up_runs);

  double gbyte = (float)(size) * sizeof(T) * 2.0 / 1e9;
  double gbytes = (gbyte * 1e3) / avg_ex_time;

  return {(double)avg_ex_time, gbyte, gbytes};
}

std::vector<double> run_gmem_fp32_v2() { return run_gmem_v2<float>(); }

std::vector<double> run_gmem_fp64_v2() { return run_gmem_v2<double>(); }