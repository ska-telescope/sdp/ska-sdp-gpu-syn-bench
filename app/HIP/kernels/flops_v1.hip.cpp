#include "../util.hip.hpp"

template <typename T> __global__ void kernel_fma_v1(T *ptr) {
  T x, y;

  if constexpr (std::is_same_v<T, __half2>) {
    x = {(float)threadIdx.x, (float)threadIdx.x};
    y = {0, 0};
  } else {
    x = threadIdx.x;
    y = 0;
  }

  for (int i = 0; i < 2048; i++) {
    x = x * y + x;
    y = y * x + y;
  }

  ptr[blockIdx.x * blockDim.x + threadIdx.x] = x + y;
}

template <typename T> std::vector<double> run_fp_v1() {
  std::vector<int> dim = get_launch_kernel_dimensions();
  int nr_warm_up_runs = get_env_var("NR_WARM_UP_RUNS", 2);
  int nr_iterations = get_env_var("NR_ITERATIONS", 5);

#ifdef DEBUG
  print_device_info();
  std::cout << "Dimensions: <<" << dim[0] << ", " << dim[1] << ">>"
            << std::endl;
  std::cout << "NR_WARM_UP_RUNS: " << nr_warm_up_runs << std::endl;
  std::cout << "NR_ITERATIONS: " << nr_iterations << std::endl;
#endif
  dim[0] *= 2048;
  int size = dim[0] * dim[1];

  T *d_ptr;

  hipEvent_t start, stop;
  hipCheck(hipEventCreate(&start));
  hipCheck(hipEventCreate(&stop));

  hipCheck(hipMalloc(&d_ptr, size * sizeof(T)));

  std::vector<float> ex_time;

  for (int i = 0; i < nr_iterations + nr_warm_up_runs; i++) {
    hipCheck(hipEventRecord(start));

    hipLaunchKernelGGL(kernel_fma_v1, dim[0], dim[1], 0, 0, d_ptr);

    hipCheck(hipEventRecord(stop));

    hipCheck(hipEventSynchronize(stop));

    float milliseconds = 0;
    hipCheck(hipEventElapsedTime(&milliseconds, start, stop));

    ex_time.push_back(milliseconds);
  }

  hipCheck(hipFree(d_ptr));

  float avg_ex_time =
      std::accumulate(ex_time.begin() + nr_warm_up_runs, ex_time.end(), 0.0) /
      (ex_time.size() - nr_warm_up_runs);

  double gflop;
  if constexpr (std::is_same_v<T, __half2>) {
    gflop = 1e-9 * (dim[0] * dim[1]) * (2 * 2 * 2048) * 2;
  } else {
    gflop = 1e-9 * (dim[0] * dim[1]) * (2 * 2 * 2048);
  }
  double gflops = (gflop * 1e3) / avg_ex_time;

  return {(double)avg_ex_time, gflop, gflops};
}

std::vector<double> run_fp16_2_v1() { return run_fp_v1<__half2>(); }

std::vector<double> run_fp32_v1() { return run_fp_v1<float>(); }

std::vector<double> run_fp64_v1() { return run_fp_v1<double>(); }