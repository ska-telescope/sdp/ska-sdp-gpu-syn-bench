#pragma once

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

unsigned roundToPowOf2(unsigned number);

int get_env_var(const char *env_var, int default_value);

void print_stats(std::string title, std::vector<double> stats, int sel);

void print_t_stats(std::string title, double stat, int sel);

void print_benchmark();

extern std::vector<double> run_gmem_fp32_v1();
extern std::vector<double> run_gmem_fp64_v1();
extern std::vector<double> run_gmem_fp32_v2();
extern std::vector<double> run_gmem_fp64_v2();
extern std::vector<double> run_l2_fp32_v1();
extern std::vector<double> run_l2_fp64_v1();
extern std::vector<double> run_l1_fp32_v1();
extern std::vector<double> run_l1_fp64_v1();
extern std::vector<double> run_fp16_2_v1();
extern std::vector<double> run_fp32_v1();
extern std::vector<double> run_fp64_v1();

extern void extern_print_device_info();
extern std::string extern_get_device_name();