#include "util.hpp"

std::vector<double> run_gmem_fp64_v1() {
  int nr_warm_up_runs = get_env_var("NR_WARM_UP_RUNS", 2);
  int nr_iterations = get_env_var("NR_ITERATIONS", 5);
  std::vector<int> dim = cl::get_launch_kernel_dimensions();

#ifdef DEBUG
  print_device_info();
  std::cout << "Dimensions: <<" << dim[0] << ", " << dim[1] << ">>"
            << std::endl;
  std::cout << "NR_WARM_UP_RUNS: " << nr_warm_up_runs << std::endl;
  std::cout << "NR_ITERATIONS: " << nr_iterations << std::endl;
#endif

  cl::Context context;
  std::vector<cl::Device> devices;

  cl::init(context, devices);
  cl::Device &device = devices[0];

  cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);

  std::string source_file_name = "gmem_fp64_v1.cl";
  std::string kernel_name = "kernel_gmem_fp64_v1";

  cl::Kernel kernel = cl::compile_program_kernel(context, device,
                                                 source_file_name, kernel_name);

  int maxItems = cl::get_gmem_size() / 3;

  float numItems = (float)roundToPowOf2(maxItems) / sizeof(double);
  dim[0] = numItems;
  int size = dim[0];

  // Kernel dimensions
  cl::NDRange global_size = numItems;
  cl::NDRange local_size = dim[1];

#ifdef DEBUG
  std::cout << "global_size[0] " << global_size[0] << std::endl;
  std::cout << "local_size[0] " << local_size[0] << std::endl;
#endif

  cl::Buffer buffer1 = cl::Buffer(context, CL_MEM_READ_ONLY,
                                  (global_size[0] * sizeof(cl_double)));
  cl::Buffer buffer2 = cl::Buffer(context, CL_MEM_READ_ONLY,
                                  (global_size[0] * sizeof(cl_double)));
  cl::Buffer buffer3 = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                                  (global_size[0] * sizeof(cl_double)));

  kernel.setArg(0, buffer1);
  kernel.setArg(1, buffer2);
  kernel.setArg(2, buffer3);

  std::vector<double> ex_time;

  cl::Event start, end;
  for (int i = 0; i < nr_iterations + nr_warm_up_runs; i++) {
    queue.enqueueMarkerWithWaitList(NULL, &start);
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size);
    queue.enqueueMarkerWithWaitList(NULL, &end);

    queue.finish();
    double milliseconds = cl::compute_runtime(start, end);
    ex_time.push_back(milliseconds);
  }

  float avg_ex_time =
      std::accumulate(ex_time.begin() + nr_warm_up_runs, ex_time.end(), 0.0) /
      (ex_time.size() - nr_warm_up_runs);

  double gbyte = (float)(size) * sizeof(double) * 3.0 / 1e9;
  double gbytes = gbyte / avg_ex_time;

  return {avg_ex_time * 1e3, gbyte, gbytes};
}