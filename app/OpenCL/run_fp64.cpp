#include "util.hpp"

std::vector<double> run_fp64_v1() {
  int nr_warm_up_runs = get_env_var("NR_WARM_UP_RUNS", 2);
  int nr_iterations = get_env_var("NR_ITERATIONS", 5);
  std::vector<int> dim = cl::get_launch_kernel_dimensions();

  dim[0] *= 2048;

#ifdef DEBUG
  print_device_info();
  std::cout << "Dimensions: <<" << dim[0] << ", " << dim[1] << ">>"
            << std::endl;
  std::cout << "NR_WARM_UP_RUNS: " << nr_warm_up_runs << std::endl;
  std::cout << "NR_ITERATIONS: " << nr_iterations << std::endl;
#endif

  cl::Context context;
  std::vector<cl::Device> devices;

  cl::init(context, devices);
  cl::Device &device = devices[0];

  cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);

  std::string source_file_name = "fp64.cl";
  std::string kernel_name = "kernel_fp";

  cl::Kernel kernel = cl::compile_program_kernel(context, device,
                                                 source_file_name, kernel_name);

  // Kernel dimensions
  cl::NDRange global_size = dim[0] * dim[1];
  cl::NDRange local_size = dim[1];

#ifdef DEBUG
  std::cout << "global_size[0] " << global_size[0] << std::endl;
  std::cout << "local_size[0] " << local_size[0] << std::endl;
#endif

  cl::Buffer buffer = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                                 (global_size[0] * sizeof(cl_double)));

  kernel.setArg(0, buffer);

  std::vector<double> ex_time;

  cl::Event start, end;
  for (int i = 0; i < nr_iterations + nr_warm_up_runs; i++) {
    queue.enqueueMarkerWithWaitList(NULL, &start);
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size);
    queue.enqueueMarkerWithWaitList(NULL, &end);

    queue.finish();
    double milliseconds = cl::compute_runtime(start, end);
    ex_time.push_back(milliseconds);
  }

  float avg_ex_time =
      std::accumulate(ex_time.begin() + nr_warm_up_runs, ex_time.end(), 0.0) /
      (ex_time.size() - nr_warm_up_runs);

  auto gflop = 1e-9 * (global_size[0]) * (2 * 2 * 2048);
  auto gflops = (gflop) / avg_ex_time;

  return {avg_ex_time * 1e3, gflop, gflops};
}