#pragma OPENCL EXTENSION cl_khr_fp16 : enable
__kernel void kernel_fp(__global half2 *ptr) {

  half2 x = {(half)get_local_id(0), (half)get_local_id(0)};
  half2 y = {0, 0};

  for (int i = 0; i < 2048; i++) {
    x = x * y + x;
    y = y * x + y;
  }

  ptr[get_global_id(0)] = y;
}