kernel void kernel_gmem_fp64_v1(__global double *a, __global double *b,
                                __global double *c) {
  int id = get_global_id(0);
  c[id] = a[id] + b[id];
}