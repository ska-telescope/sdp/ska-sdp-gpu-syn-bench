kernel void kernel_gmem_fp32_v2(__global float *a, __global float *c) {
  int id = get_global_id(0);
  c[id] = a[id];
}