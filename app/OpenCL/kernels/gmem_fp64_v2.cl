kernel void kernel_gmem_fp64_v2(__global double *a, __global double *c) {
  int id = get_global_id(0);
  c[id] = a[id];
}