__kernel void kernel_fp(__global double *ptr) {

  double x = (double)get_local_id(0);
  double y = 0;

  for (int i = 0; i < 2048; i++) {
    x = x * y + x;
    y = y * x + y;
  }

  ptr[get_global_id(0)] = y;
}