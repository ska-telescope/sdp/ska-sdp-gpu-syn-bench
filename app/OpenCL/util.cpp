#include "util.hpp"

std::string extern_get_device_name() { return get_device_name(); }

void extern_print_device_info() { print_device_info(); }

void print_benchmark() {
  std::cout << ">>> OpenCL Theoretical performance and bandwidth" << std::endl;
}

inline std::string get_device_name() {
  cl::Context context;
  std::vector<cl::Device> devices;
  cl::init(context, devices);
  cl::Device &device = devices[0];
  std::string name = device.getInfo<CL_DEVICE_NAME>();
  std::replace(name.begin(), name.end(), ' ', '_');
  return name;
}

inline void print_device_info() {
  cl::Context context;
  std::vector<cl::Device> devices;
  cl::init(context, devices);
  cl::Device &device = devices[0];

  std::cout << std::endl;
  std::cout << "Device Name " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::cout << "  Memory Clock Rate (MHz): "
            << device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << std::endl;
  std::cout << "  Memory size (GB): "
            << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() * 1e-9 << std::endl;
  std::cout << "  Streaming MultiProcessors (SMs): "
            << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
  std::cout << std::endl;
}

namespace cl {
void print_platform(cl::Platform &platform) {
  std::cout << ">>> Platform: " << std::endl;
  std::cout << "Name       : " << platform.getInfo<CL_PLATFORM_NAME>()
            << std::endl;
  std::cout << "Version    : " << platform.getInfo<CL_PLATFORM_VERSION>()
            << std::endl;
  std::cout << "Extensions : " << platform.getInfo<CL_PLATFORM_EXTENSIONS>()
            << std::endl;
  std::cout << std::endl;
}
void print_device(cl::Device &device, bool marker) {
  std::cout << ">>> Device: ";
  if (marker)
    std::cout << " (selected)";
  std::cout << endl;
  std::cout << "Name            : " << device.getInfo<CL_DEVICE_NAME>()
            << std::endl;
  std::cout << "Driver version  : " << device.getInfo<CL_DRIVER_VERSION>()
            << std::endl;
  std::cout << "Device version  : " << device.getInfo<CL_DEVICE_VERSION>()
            << std::endl;
  std::cout << "Compute units   : "
            << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
  std::cout << "Clock frequency : "
            << device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << " MHz"
            << std::endl;
  std::cout << "Global memory   : "
            << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() * 1e-9 << " Gb"
            << std::endl;
  std::cout << "Local memory    : "
            << device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() * 1e-6 << " Mb"
            << std::endl;
  std::cout << std::endl;
}

std::vector<int> get_launch_kernel_dimensions() {
  cl::Context context;
  std::vector<cl::Device> devices;
  cl::init(context, devices);
  cl::Device &device = devices[0];

#ifndef AMD_REDUCE_THREADS
  return {(int)device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>(),
          (int)device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>()[0]};
#else
  return {(int)device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() * 4, 256};
#endif
}

int get_gmem_size() {
  cl::Context context;
  std::vector<cl::Device> devices;
  cl::init(context, devices);
  cl::Device &device = devices[0];

  return (int)device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
}

void init(cl::Context &context, vector<cl::Device> &devices) {
  vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  // The selected device
  int i = 0;
  char *opencl_device_cstr = getenv("OPENCL_DEVICE");
  int opencl_device = opencl_device_cstr ? atoi(opencl_device_cstr) : 0;

#ifdef DEBUG
  std::cout << ">>> OpenCL environment: " << std::endl;
#endif

  // Iterate all platforms
  for (cl::Platform &platform : platforms) {
#ifdef DEBUG
    print_platform(platform);
#endif

    // Get devices forthe current platform
    vector<cl::Device> devices_;
    platform.getDevices(CL_DEVICE_TYPE_GPU, &devices_);

    // Iterate all devices
    for (cl::Device &device : devices_) {
      bool selected;
      if ((selected = (i == opencl_device))) {
        devices.push_back(device);
      }
#ifdef DEBUG
      print_device(device, selected);
      i++;
#endif
    }
  }

  if (devices.size() == 0) {
    cerr << "Could not get device " << opencl_device << endl;
    exit(EXIT_FAILURE);
  }

  context = cl::Context(devices);
}

string get_source(string &filename) {
  // Source directory
  string srcdir;

  char const *tmp = getenv("BENCH_ROOT_DIR");
  if (tmp == NULL) {
    std::cout << "BENCH_ROOT_DIR not set" << std::endl;
    std::cout << "Please run source ./share/setup-env.sh in the main folder"
              << std::endl;
  } else {
    srcdir = std::string(tmp);
  }

  // Get source filename
  stringstream source_file_name_;
  source_file_name_ << srcdir << "/app/OpenCL/kernels/" << filename;
  string source_file_name = source_file_name_.str();

  // Read kernel source from file
  ifstream source_file(source_file_name.c_str());
  string source_kernel(istreambuf_iterator<char>(source_file),
                       (istreambuf_iterator<char>()));
  source_file.close();

  // Construct full source file
  stringstream full_source;
  full_source << source_kernel;
  return full_source.str();
}

string get_flags() { return string("-cl-fast-relaxed-math"); }

cl::Program compile_program(cl::Context &context, cl::Device &device,
                            string &source) {
  string flags = get_flags();

#ifdef DEBUG
  cout << ">>> Compiling:" << endl << "Flags  : " << flags << endl << endl;
  cout << source << endl;
#endif

  cl::Program program;
  bool failure = false;

  try {
    program = cl::Program(context, source);
    vector<cl::Device> devices;
    devices.push_back(device);
    program.build(devices, flags.c_str());
  } catch (cl::Error &error) {
    failure = true;
    cerr << "Compilation failed: " << error.what() << endl;
  }

  string msg;
  program.getBuildInfo(device, CL_PROGRAM_BUILD_LOG, &msg);

#ifdef DEBUG
  cout << ">>> Compiler output:" << endl << msg << endl << endl;
#endif

  if (failure) {
    // Temporary fix to let the benchmark run even when fp16 fails
    return program; // exit(EXIT_FAILURE);
  } else {
    return program;
  }
}

cl::Kernel get_kernel(cl::Program &program, string &name) {
#ifdef DEBUG
  std::cout << ">>> Loading kernel: " << name << std::endl;
#endif
  try {
    return cl::Kernel(program, name.c_str());
  } catch (cl::Error &error) {
    cerr << "Loading kernel failed: " << error.what() << endl;
    // Temporary fix to let the benchmark run even when fp16 fails
    return cl::Kernel(); // exit(EXIT_FAILURE);
  }
}

cl::Kernel compile_program_kernel(cl::Context context, cl::Device device,
                                  string source_filename, string kernel_name) {
  string source = get_source(source_filename);

  cl::Program program = compile_program(context, device, source);

  return get_kernel(program, kernel_name);
}

double compute_runtime(cl::Event &start, cl::Event &end) {
  double runtime = 0;
  runtime -= start.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  runtime += end.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  return runtime * 1e-9;
}
} // namespace cl