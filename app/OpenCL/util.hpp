#pragma once

#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 210
#include <CL/opencl.hpp>

#include "bench-common.hpp"

using namespace std;

namespace cl {

void print_platform(cl::Platform &platform);

void print_device(cl::Device &device, bool marker);

std::vector<int> get_launch_kernel_dimensions();

int get_gmem_size();

void init(cl::Context &context, vector<cl::Device> &devices);

string get_source(string &filename);

string get_flags();

cl::Program compile_program(cl::Context &context, cl::Device &device,
                            string &source);

cl::Kernel get_kernel(cl::Program &program, string &name);

cl::Kernel compile_program_kernel(cl::Context context, cl::Device device,
                                  string source_filename, string kernel_name);

double compute_runtime(cl::Event &start, cl::Event &end);
} // namespace cl

inline std::string get_device_name();

inline void print_device_info();