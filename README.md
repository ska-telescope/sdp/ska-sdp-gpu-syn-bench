# GPU synthetic benchmark 

This benchmark includes kernels in CUDA, HIP, and OpenCL with AMD and NVIDIA GPUs support.
It computes the empirical peak half-/single-/double-precision performance and memory bandwidth.

# Installation

Make sure to have CUDA/HIP/OpenCL support on your system.
Source the setup file(s) in the **share** folder. 

```
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=../install
```

Check the benchmark you would like to build with:

```
ccmake .
```

Then install:
```
make -j
make install
```

### Run benchmark

Simply run:
```
./<version>-peak
```
or, if you need to save the results in CSV format:
```
./<version>-peak_csv <folder_path>
```
where:
- `<version>` can be `cuda`, `hip`, or `opencl`.
- `<folder_path>` is the location where the CSV file is saved (it will be named after the GPU name and the benchmark version).

### Environment variables

- `NR_WARM_UP_RUNS`: number of warm-up runs per benchmark 
- `NR_ITERATIONS`: number of iterations per benchmark (in addition to the warm up runs)

### Results

Some results on different GPUs are reported in the **res** folder.

### Note
- Half precision is not working now in the OpenCL version.
- L2 cache bandwidth is currently running only in the CUDA version.
- Cache bandwidth is not supported in the OpenCL version.


### Based on:
- https://gitlab.com/astron-idg/idg-cuda-bench
- https://github.com/krrishnarraj/clpeak
- https://gitlab.com/astron-misc/clpeak 
- https://gitlab.com/astron-misc/cudapeak
- https://arxiv.org/pdf/1804.06826.pdf
